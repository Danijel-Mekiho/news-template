<?php
    require_once('config.php');

    $masterFile = json_decode(file_get_contents("master-file.json"));

    foreach($masterFile as $pageName){
        $page = fopen(__DIR__ . '/pages/'."$pageName".'.php', 'w') or die('Unable to file open');
        $template = json_decode(file_get_contents( __DIR__ . '/templates/'.$pageName.'.json' ));
        genertePages($page, $template);
    }

    function genertePages($page, $template){
        global $headerHTML;
        global $footerHTML;
        $sections = $template->sections;

        $content = $headerHTML;
        
        //curate conent by each template
        require_once(__DIR__ . '/templates/'.$template->layout.'.php');

        switch($template->layout){
            case "BitcoinPageLayout":
                $ob = new BitcoinPageLayout($sections);
                break;
            case "WalletPageLayout":
                $ob = new WalletPageLayout($sections);
                break;
        }

        // print_r($ob->curate());

        $content .= $ob->curate();

        $content .= $footerHTML;
        
        fwrite($page, $content);
        fclose($page);
    }
    header('Location: http://localhost/news-template/pages/'.$masterFile[0].'.php');
?>