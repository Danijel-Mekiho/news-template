<?php        
    ini_set('max_execution_time', 30000000);
    
    $headerHTML = '
<!DOCTYPE html>
<html  lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--Favicon-->
        <link rel="icon" href="../assets/images/favicon.ico" type="image/x-icon">
        
        <title>Prototype</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--   CSS   -->
	
        <!-- Bootstrap -->
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <!-- Template styles-->
        <link rel="stylesheet" href="../assets/css/style.css">
        <!-- Responsive styles-->
        <link rel="stylesheet" href="../assets/css/responsive.css">
        <!-- FontAwesome -->
        <link rel="stylesheet" href="../assets/css/font-awesome.min.css">
        <!-- Owl Carousel -->
        <link rel="stylesheet" href="../assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="../assets/css/owl.theme.default.min.css">
        <!-- Colorbox -->
        <link rel="stylesheet" href="../assets/css/colorbox.css">

    </head>

    <body>
    ';

    $footerHTML = '
        <!-- Javascript Files  -->

        <!-- initialize jQuery Library -->
        <script type="text/javascript" src="../assets/js/jquery.js"></script>
        <!-- Popper Jquery -->
        <script type="text/javascript" src="../assets/js/popper.min.js"></script>
        <!-- Bootstrap jQuery -->
        <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
        <!-- Owl Carousel -->
        <script type="text/javascript" src="../assets/js/owl.carousel.min.js"></script>
        <!-- Color box -->
        <script type="text/javascript" src="../assets/js/jquery.colorbox.js"></script>
        <!-- Smoothscroll -->
        <script type="text/javascript" src="../assets/js/smoothscroll.js"></script>


        <!-- Template custom -->
        <script type="text/javascript" src="../assets/js/custom.js"></script>    

    </body>
</html>
    ';
    
    function BingNewsSearch ($query, $limit) {
        $accessKey = 'efe9cede36b04a2e9ccf4c104f262a65';    
        $endpoint = 'https://api.cognitive.microsoft.com/bing/v7.0/news';

        $headers = "Ocp-Apim-Subscription-Key: $accessKey\r\n";
        $options = array ('http' => array (
                            'header' => $headers,
                            'method' => 'GET' ));
        $context = stream_context_create($options);
        $endpoint .= "/search?q=" . urlencode($query).$limit;
        $result = file_get_contents($endpoint, false, $context);
        return json_decode($result)->value;
    }
?>