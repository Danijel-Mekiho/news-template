<?php
    require_once('config.php');

    class BitcoinPageLayout{
        function __construct($sections){
            $this->sections = $sections;
            $this->content = '
        <div class="trending-bar d-md-block d-lg-block d-none">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="trending-title">Trending Now</h3>
                        <div id="trending-slide" class="owl-carousel owl-theme trending-slide">
                            <div class="item">
                                <div class="post-content">
                                    <h2 class="post-title title-small">
                                        <a href="#">The best MacBook Pro alternatives in 2017 for Apple users</a>
                                    </h2>
                                </div><!-- Post content end -->
                            </div><!-- Item 1 end -->
                            <div class="item">
                                <div class="post-content">
                                    <h2 class="post-title title-small">
                                        <a href="#">Soaring through Southern Patagonia with the Premium Byrd drone</a>
                                    </h2>
                                </div><!-- Post content end -->
                            </div><!-- Item 2 end -->
                            <div class="item">
                                <div class="post-content">
                                    <h2 class="post-title title-small">
                                        <a href="#">Super Tario Run isn’t groundbreaking, but it has Mintendo charm</a>
                                    </h2>
                                </div><!-- Post content end -->
                            </div><!-- Item 3 end -->
                        </div><!-- Carousel end -->
                    </div><!-- Col end -->
                </div><!--/ Row end -->
            </div><!--/ Container end -->
        </div><!--/ Trending end -->
    
        <div class="main-nav clearfix">
            <div class="container">
                <div class="row">
                    <nav class="navbar navbar-expand-lg col">
                        <div class="site-nav-inner float-left">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <!-- End of Navbar toggler -->

                            <div id="navbarSupportedContent" class="collapse navbar-collapse navbar-responsive-collapse">
                                <ul class="nav navbar-nav">
                                    <li class="nav-item dropdown active">
                                        <a href="#">Home</a>
                                    </li>

                                    <li>
                                        <a href="firstPage.php">Lifestyle</a>
                                    </li><!-- Tab menu end -->

                                    <li>
                                        <a href="#">Technology</a>
                                    </li>

                                    <li>
                                        <a href="#">Video</a>
                                    </li><!-- Video menu end -->

                                    <li>
                                        <a href="#">Travel</a>
                                    </li>

                                    <li>
                                        <a href="#">Features</a>
                                    </li><!-- Features menu end -->
                                </ul><!--/ Nav ul end -->
                            </div><!--/ Collapse end -->

                        </div><!-- Site Navbar inner end -->
                    </nav><!--/ Navigation end -->

                    <div class="nav-search">
                        <span id="search"><i class="fa fa-search"></i></span>
                    </div><!-- Search end -->
                    
                    <div class="search-block" style="display: none;">
                        <input type="text" class="form-control" placeholder="Type what you want and enter">
                        <span class="search-close">&times;</span>
                    </div><!-- Site search end -->

                </div><!--/ Row end -->
            </div><!--/ Container end -->

        </div><!-- Menu wrapper end -->

        <div class="gap-40"></div>
        ';
        }

        function curate(){
            $section_no = 0;

            foreach($this->sections as $section_name=>$value){
                $section_class = 'class="block-wrapper"';
                if($section_no==0) $section_class = 'class="featured-post-area no-padding"';
                $this->content .= '
        <section id="'.$section_name.'" '.$section_class.'>';
        
                foreach($value as $keyword){
                    // $limit = '&count=1';
                    $limit = '';
                    $json = BingNewsSearch($keyword, $limit);

                    foreach($json as $article){
                        $imageUrl = isset($article->image) ? $article->image->thumbnail->contentUrl : '';
                        $this->content .= '            
            <div class="'.$keyword.' row mx-5">
                <img src="'.$imageUrl.'"/>
                <div class="col-sm-6 article">
                    <a target="_blank" href="'.$article->url.'"> '.$article->name.' </a>
                    <p>  Keyword: '.$keyword.' </p>
                    <h4> '.$article->description.' </h4>
                </div>
            </div>';     
                    }

                }

                $this->content .= '
        </section>
                ';

                $section_no++;
            }

            return $this->content;
        }

        
        // //     foreach(json_decode($json)->value as $article){
        // //         $imageUrl = isset($article->image) ? $article->image->thumbnail->contentUrl : '';
        // //         $content .= '            
        // //     <div class="'.$keyword.' row mx-5">
        // //         <img src="'.$imageUrl.'"/>
        // //         <div class="col-sm-6 article">
        // //             <a target="_blank" href="'.$article->url.'"> '.$article->name.' </a>
        // //             <p>  Keyword: '.$keyword.' </p>
        // //             <h4> '.$article->description.' </h4>
        // //         </div>
        // //     </div>';            
        // //     }
    }
?>