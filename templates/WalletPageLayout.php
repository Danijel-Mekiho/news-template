<?php
    class WalletPageLayout{
        function __construct($sections){
            $this->sections = $sections;
            $this->content = '
        <div class="trending-bar d-md-block d-lg-block d-none">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="trending-title">Trending Now</h3>
                        <div id="trending-slide" class="owl-carousel owl-theme trending-slide">
                            <div class="item">
                                <div class="post-content">
                                    <h2 class="post-title title-small">
                                        <a href="#">The best MacBook Pro alternatives in 2017 for Apple users</a>
                                    </h2>
                                </div><!-- Post content end -->
                            </div><!-- Item 1 end -->
                            <div class="item">
                                <div class="post-content">
                                    <h2 class="post-title title-small">
                                        <a href="#">Soaring through Southern Patagonia with the Premium Byrd drone</a>
                                    </h2>
                                </div><!-- Post content end -->
                            </div><!-- Item 2 end -->
                            <div class="item">
                                <div class="post-content">
                                    <h2 class="post-title title-small">
                                        <a href="#">Super Tario Run isn’t groundbreaking, but it has Mintendo charm</a>
                                    </h2>
                                </div><!-- Post content end -->
                            </div><!-- Item 3 end -->
                        </div><!-- Carousel end -->
                    </div><!-- Col end -->
                </div><!--/ Row end -->
            </div><!--/ Container end -->
        </div><!--/ Trending end -->
    
        <div class="main-nav clearfix">
            <div class="container">
                <div class="row">
                    <nav class="navbar navbar-expand-lg col">
                        <div class="site-nav-inner float-left">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <!-- End of Navbar toggler -->

                            <div id="navbarSupportedContent" class="collapse navbar-collapse navbar-responsive-collapse">
                                <ul class="nav navbar-nav">
                                    <li class="nav-item dropdown active">
                                        <a href="#">Home</a>
                                    </li>

                                    <li>
                                        <a href="firstPage.php">Lifestyle</a>
                                    </li><!-- Tab menu end -->

                                    <li>
                                        <a href="#">Technology</a>
                                    </li>

                                    <li>
                                        <a href="#">Video</a>
                                    </li><!-- Video menu end -->

                                    <li>
                                        <a href="#">Travel</a>
                                    </li>

                                    <li>
                                        <a href="#">Features</a>
                                    </li><!-- Features menu end -->
                                </ul><!--/ Nav ul end -->
                            </div><!--/ Collapse end -->

                        </div><!-- Site Navbar inner end -->
                    </nav><!--/ Navigation end -->

                    <div class="nav-search">
                        <span id="search"><i class="fa fa-search"></i></span>
                    </div><!-- Search end -->
                    
                    <div class="search-block" style="display: none;">
                        <input type="text" class="form-control" placeholder="Type what you want and enter">
                        <span class="search-close">&times;</span>
                    </div><!-- Site search end -->

                </div><!--/ Row end -->
            </div><!--/ Container end -->

        </div><!-- Menu wrapper end -->

        <div class="gap-40"></div>';

        }

        function curate(){
            $section_no = 0;
            $limit = '&count=3';
            $content = "";
            // $limit = '';

            foreach($this->sections as $section_name=>$value){
                $section_class = 'class="block-wrapper"';
                if($section_no==0) $section_class = 'class="featured-post-area no-padding"';
                if ($section_no!=2) $content .= '

        <section id="'.$section_name.'" '.$section_class.'>'; 
                
                
                // main section
                if($section_no==0){   
                    $content .= '
            <div class="container">
                <div class="row">';

                    $keyword_no = 0;
                    foreach($value as $keyword){
                        $class = $keyword_no==0 ? "col-lg-7 col-md-12 pad-r" : "col-lg-5 col-md-12 pad-l";
                        $content .= '
                    <div class='.$class.'>
                        <div id="featured-slider" class="owl-carousel owl-theme featured-slider">';  

                        $news = BingNewsSearch($keyword, $limit);
                        $data = json_encode($news, JSON_PRETTY_PRINT);
                        // print_r("<pre>".$data."</pre>");
                        foreach($news as $article){
                            $imageUrl = isset($article->image) ? $article->image->thumbnail->contentUrl : '';
                            $content .= '
                                <div class="item" style="background-image:url('.$imageUrl.')">
                                    <div class="featured-post">
                                        <div class="post-content">
                                            <a class="post-cat text-white">'.$keyword.'</a>
                                            <h2 class="post-title title-extra-large">
                                                <a target="_blank" href="'.$article->url.'">'.$article->name.'</a>
                                            </h2>
                                            <h2 class="post-title title-small">
                                                <a href="#">'.$article->description.'</a>
                                            </h2>
                                             <span class="post-date">'.date("M d, Y",strtotime($article->datePublished)).'</span>
                                        </div>
                                    </div>
                                </div>';

                        }

                        $content .= '
                        </div>
                    </div>';
                        $keyword_no++;
                    }      

                    $content .= '
                </div>
            </div>';
                }

                // sub1 section & sub2 section

                else if($section_no==1 || $section_no==2){   
                    if($section_no==1){
                        $content .= ' 
            <div class="container">
                <div class="row">               
                    <div class="col-lg-8 col-md-12">
                        <div class="latest-news block color-red">
                            <h3 class="block-title"><span>'.$section_name.'</span></h3>
                            <div id="latest-news-slide" class="owl-carousel owl-theme latest-news-slide">
                        ';

                        $keyword_no = 0;
                        foreach($value as $keyword){
                            $content .= '
                                <div class="item">
                                    <ul class="list-post">';  

                            $news = BingNewsSearch($keyword, $limit);
                            $data = json_encode($news, JSON_PRETTY_PRINT);
                            // print_r("<pre>".$data."</pre>");
                            foreach($news as $article){
                                $imageUrl = isset($article->image) ? $article->image->thumbnail->contentUrl : '';
                                $content .= '
                                        <li class="clearfix">
                                            <div class="post-block-style clearfix">
                                                <div class="post-thumb">
                                                    <a href="#"><img class="img-fluid" src="'.$imageUrl.'" alt="" /></a>
                                                </div>
                                                <a class="post-cat" href="#">'.$keyword.'</a>
                                                <div class="post-content">
                                                    <h2 class="post-title title-large">
                                                        <a target="_blank" href="'.$article->url.'">'.$article->name.'</a>
                                                    </h2>
                                                    <h2 class="post-title title-small">
                                                        <a href="#"  class="text-dark font-weight-light">'.$article->description.'</a>
                                                    </h2>
                                                    <div class="post-meta">                                                        
                                                        <span class="post-date">'.date("M d, Y",strtotime($article->datePublished)).'</span>
                                                    </div>
                                                </div><!-- Post content end -->
                                            </div><!-- Post Block style end -->
                                        </li><!-- Li end -->
                                        
                                        <div class="gap-30"></div>';

                            }
                            // <span class="post-author"><a href="#">'.$article->provider->name.'</a></span>
                            $content .= '
                                    </ul>
                                </div>';
                            $keyword_no++;
                        }  

                        $content .= '
                            </div>
                        </div>
                    </div>
                        ';
                    }else{
                        $content .= '
                    <div class="col-lg-4 col-md-12">
                        <div class="sidebar sidebar-right">
                        ';

                        $keyword_no = 0;
                        foreach($value as $keyword){
                            $content .= '
                            <div class="widget color-orange">
                                <h3 class="block-title"><span>'.$keyword.'</span></h3>
                                <div class="list-post-block">
                                    <ul class="list-post">';  

                            $news = BingNewsSearch($keyword, $limit);
                            $data = json_encode($news, JSON_PRETTY_PRINT);
                            // print_r("<pre>".$data."</pre>");
                            $article_no = 0;
                            foreach($news as $article){
                                $imageUrl = isset($article->image) ? $article->image->thumbnail->contentUrl : '';
                                $content .= '
                                        <li class="clearfix">
                                            <div class="post-block-style post-float clearfix">
                                                <div class="post-thumb">
                                                    <a href="#"><img class="img-fluid" src="'.$imageUrl.'" alt="" /></a>
                                                    <a class="post-cat" href="#">'.$keyword.'</a>
                                                </div>
                                                <div class="post-content">
                                                    <h2 class="post-title title-medium">
                                                        <a target="_blank" href="'.$article->url.'">'.$article->name.'</a>
                                                    </h2>
                                                    <div class="post-meta">                                                        
                                                        <span class="post-date">'.date("M d, Y",strtotime($article->datePublished)).'</span>
                                                    </div>
                                                </div><!-- Post content end -->
                                            </div><!-- Post Block style end -->                                            
                                            <h2 class="post-title title-small mb-5">
                                                <a class="text-dark font-weight-normal">'.$article->description.'</a>
                                            </h2>
                                        </li><!-- Li end -->';

                                $article_no++;

                            }
                            $content .= '
                                    </ul>
                                </div>
                            </div>';
                            $keyword_no++;
                        }  


                        $content .= '
                        </div>
                    </div>
                </div>
            </div>
                        ';
                    }    
                }
                
            // sub3 section
                else if($section_no==3){   
                    $content .= '
                <div class="container">
                    <div class="row">';
    
                        $keyword_no = 0;
                        foreach($value as $keyword){
                            $content .= '
                        <div class="col-lg-6">
                            <div class="block color-dark-blue">
                                <h3 class="block-title"><span>'.$keyword.'</span></h3>
                                <div class="list-post-block">
                                    <ul class="list-post">';  
    
                            $news = BingNewsSearch($keyword, $limit);
                            $data = json_encode($news, JSON_PRETTY_PRINT);
                            // print_r("<pre>".$data."</pre>");
                            foreach($news as $article){
                                $imageUrl = isset($article->image) ? $article->image->thumbnail->contentUrl : '';
                                $content .= '
                                        <li class="clearfix">
                                            <div class="post-block-style post-float clearfix">
                                                <div class="post-thumb">
                                                    <a href="#">
                                                        <img class="img-fluid" src="'.$imageUrl.'" alt="" />
                                                    </a>
                                                </div><!-- Post thumb end -->

                                                <div class="post-content">
                                                    <h2 class="post-title title-medium">
                                                        <a target="_blank" href="'.$article->url.'">'.$article->name.'</a>
                                                    </h2>
                                                    <h2 class="post-title title-small">
                                                        <a href="#"  class="text-secondary font-italic">'.$article->description.'</a>
                                                    </h2>
                                                    <div class="post-meta">
                                                        <span class="post-date">'.date("M d, Y",strtotime($article->datePublished)).'</span>
                                                    </div>
                                                </div><!-- Post content end -->
                                            </div><!-- Post block style end -->
                                        </li><!-- Li 1 end -->';
    
                            }
    
                            $content .= '
                                    </ul>
                                </div>
                            </div>
                        </div>';
                            $keyword_no++;
                        }      
    
                        $content .= '
                    </div>
                </div>';
                }

                if ($section_no!=1) $content .= '
        </section>
                ';

                $section_no++;
            }

            $this->content .= $content;

            return $this->content;
        }
    }
?>